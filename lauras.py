import logging
import time
from tred import TemporalGraph, Problem


logging.basicConfig()
# Print timing information
#logging.getLogger().setLevel(logging.INFO)

edges = [("1", "2", 4),
         ("2", "3", 3),
         ("3", "4", 4),
         ("4", "5", 3),
         ("5", "6", 4),
         ("6", "7", 3),
]

for i in range(2, 8):
    edges.append((f"{i}", f"{i}_a", 1))
    for j in range(1, 4):
        edges.append((f"{i}_a", f"{i}_j", 2))


g = TemporalGraph.fromNamedVerts(edges)

print(f"{g.numVerts=} {g.numEdges=}")

#for k in range(0, 6):
#    numReached, reachable, edgesRemoved = p.findMaxReachable(k)
#    print(f"With {k=}, {numReached=}, {reachable=}, {edgesRemoved=}")
#    for delta in range(1, 2):
#        #biggest = p.find_robustness(edgesRemoved, delta)
#        biggest = g.findRobust(edgesRemoved, delta)
#        print(f"\tThis is ({delta}, {biggest-numReached})-robust")
for k in range(0, 6):
    numReached, reachable, edgesRemoved = g.kDelMaxReachability(k)
    print(f"With {k=}, {numReached=}, {reachable=}, {edgesRemoved=}")
    for delta in range(1, 2):
#        beta = 0
#        bReachable = []
#        bRemoved = []
#        bPerturbed = None
#        obReachable = []
#        time1 = time.monotonic_ns()
#        for perturbed in g.perturbations(delta):
#            pReached, pReachable, pRemoved = perturbed.kDelMaxReachability(k)
#            oReached, oReachable = perturbed.maxReachability(edgesRemoved)
#            this_beta = oReached - pReached
#            if this_beta > beta:
#                beta = this_beta
#                bReachable = pReachable
#                bRemoved = pRemoved
#                bPerturbed = perturbed
#                obReachable = oReachable
#        print(f"----------- Old version ---------------")
#        print(f"\tThis is ({delta}, {beta})-robust.")
#        if beta > 0:
#            print(f"\tOur removal:  Reachable vertices: {obReachable}")
#            print(f"\tBest removal: Reachable vertices: {bReachable} by removing {bRemoved}")
#        time2 = time.monotonic_ns()
#        print(f"This took {(time2-time1)/1e9:.3f}s")
        print(f"----------- New version ---------------")
        time1 = time.monotonic_ns()
        beta, bReachable, bRemoved, obReachable = g.robustness(edgesRemoved, delta)
        print(f"\tThis is ({delta}, {beta})-robust.")
        if beta > 0:
            print(f"\tOur removal:  Reachable vertices: {obReachable}")
            print(f"\tBest removal: Reachable vertices: {bReachable} by removing {bRemoved}")
        time2 = time.monotonic_ns()
        print(f"This took {(time2-time1)/1e9:.3f}s")
        print(f"----------- New graph   ---------------")


