
from tred.graph import TemporalGraph, TemporalEdge, Edge

import logging

import pkg_resources  # type: ignore
import minizinc  # type: ignore

# Just assuming gecode is always good, test later if other solvers are better
MZ_SOLVER = minizinc.Solver.lookup("gecode")

class Problem:

    def __init__(self, graph: TemporalGraph):
        if graph.numVerts > 20:
            raise Exception("Graph has too many vertices to brute force")
        self._graph = graph


    def _log_result(self, result):
        if result.status == minizinc.Status.OPTIMAL_SOLUTION:
            logging.info(f"Solved to optimality with initTime {result.statistics['initTime']},"
                         f" flatTime {result.statistics['flatTime']},"
                         f" solveTime {result.statistics['solveTime']}")
        else:
            logging.info(f"Solved to {result.status} with initTime {result.statistics['initTime']},"
                         f" flatTime {result.statistics['flatTime']},"
                         f" solveTime {result.statistics['solveTime']}")



    def findMaxReachable(self, k: int = 0) -> list[Edge]:
        to_remove: list[Edge] = []
        model_filename = pkg_resources.resource_filename('tred', 'minizinc-models/max_treach_kdel.mzn')
        model = minizinc.Model(model_filename)
        instance = minizinc.Instance(MZ_SOLVER, model)
        instance["nVerts"] = self._graph.numVerts
        instance["nEdges"] = self._graph.numEdges
        instance["maxTime"] = self._graph.maxTime
        instance["from"] = self._graph.edgesFrom
        instance["to"] = self._graph.edgesTo
        instance["active"] = self._graph.edgesActive
        instance["k"] = k
        result = instance.solve()
        self._log_result(result)
        return (result['numReached'],
                result['nReachable'],
                self._graph.renameEdges([e for index, e in enumerate(self._graph.edges) if not result['es'][index]])
                )

    def find_robustness(self, edgeSet: list[Edge], delta: int) -> int:
        to_remove: list[Edge] = []
        model_filename = pkg_resources.resource_filename('tred', 'minizinc-models/max_treach_perturb.mzn')
        model = minizinc.Model(model_filename)
        instance = minizinc.Instance(MZ_SOLVER, model)
        instance["nVerts"] = self._graph.numVerts
        instance["nEdges"] = self._graph.numEdges
        instance["maxTime"] = self._graph.maxTime
        instance["maxPerturb"] = delta
        instance["from"] = self._graph.edgesFrom
        instance["to"] = self._graph.edgesTo
        instance["active"] = self._graph.edgesActive
        es = [True] * self._graph.numEdges
        for index, edge in enumerate(self._graph.edges):
            if edge in edgeSet:
                es[index] = False
        instance["es"] = es
        result = instance.solve()
        self._log_result(result)
        beta = result['numReached']
        return beta


