

import concurrent.futures

import logging
from typing import Optional, Union, Iterator

import pkg_resources  # type: ignore
import minizinc  # type: ignore

# Just assuming gecode is always good, test later if other solvers are better
MZ_SOLVER = minizinc.Solver.lookup("gecode")

from tred.utils import timing

TemporalEdge = tuple[int, int, int]
NamedTemporalEdge = tuple[str, str, int]
Edge = Union[TemporalEdge, NamedTemporalEdge]


class TemporalGraph:

    def __init__(self, edges: list[TemporalEdge], nameMap: Optional[dict[str,int]] = None):
        self._edges: list[TemporalEdge] = edges
        for e in edges:
            assert type(e[0]) is int, "Vertices should be numbers"
            assert type(e[1]) is int, "Vertices should be numbers"
            assert e[0] > 0, "Vertices must be consecutive numbers starting from 1"
            assert e[1] > 0, "Vertices must be consecutive numbers starting from 1"
        self._numVerts = max(max(e[0], e[1]) for e in edges)
        self._nameMap: Optional[dict[str,int]] = nameMap

    @classmethod
    def fromNamedVerts(self, edges: list[NamedTemporalEdge]) -> 'TemporalGraph':
        numberEdges: list[TemporalEdge] = []
        nameMap: dict[str, int] = {}
        for namedEdge in edges:
            if namedEdge[0] not in nameMap:
                nameMap[namedEdge[0]] = len(nameMap) + 1
            if namedEdge[1] not in nameMap:
                nameMap[namedEdge[1]] = len(nameMap) + 1
            numberEdges.append((nameMap[namedEdge[0]],
                                nameMap[namedEdge[1]],
                                namedEdge[2]))
        return TemporalGraph(numberEdges, nameMap)

    @property
    def edges(self) -> list[TemporalEdge]:
        return self._edges

    @property
    def numEdges(self) -> int:
        return len(self._edges)

    @property
    def numVerts(self) -> int:
        return self._numVerts

    @property
    def maxTime(self) -> int:
        return max(self.edgesActive)

    @property
    def edgesFrom(self) -> list[int]:
        return [e[0] for e in self._edges]

    @property
    def edgesTo(self) -> list[int]:
        return [e[1] for e in self._edges]

    @property
    def edgesActive(self) -> list[int]:
        return [e[2] for e in self._edges]

    def incidentAway(self, vertex: int) -> list[TemporalEdge]:
        return [e for e in self._edges if e[0] == vertex]

    def renameEdges(self, edges: list[TemporalEdge]) -> list[Edge]:
        if self._nameMap is None:
            return edges
        inv_map = {v: k for k,v in self._nameMap.items()}
        res = []
        for edge in edges:
            res.append((inv_map[edge[0]], inv_map[edge[1]], edge[2]))
        return res

    def unnameEdges(self, edges: list[Edge]) -> list[TemporalEdge]:
        if not edges:
            return []
        if type(edges[0][0]) == int:
            return edges
        if self._nameMap is None:
            print(edges)
            print(type(edges[0]))
            raise Exception("Tried to turn a vertex number into a vertex name but didn't know how")
        return [(self._nameMap[e[0]], self._nameMap[e[1]], e[2]) for e in edges]

    def perturbations(self, delta: int) -> Iterator['TemporalGraph']:
        # make copy of graph
        # then adjust the edges in that copy
        #
        offsets = [-delta] * self.numEdges
        def incr(edgeIndex):
            offsets[edgeIndex] += 1
            if offsets[edgeIndex] > delta:
                offsets[edgeIndex] = 0
                if (edgeIndex + 1) == self.numEdges:
                    return False
                return incr(edgeIndex + 1)
            return True
        while incr(0):
            newEdges = [(e[0], e[1], e[2] + offsets[idx]) for idx, e in enumerate(self._edges)]
            yield TemporalGraph(newEdges)

    @timing
    def robustness(self, edgeset, delta):
        robust = 0
        bReached = []
        bRemoved = []
        oReached = []
        def get_robust_for_perturb(perturbed):
            best, bReached, bRemoved = perturbed.kDelMaxReachability(len(edgeset))
            ours, oReached = perturbed.maxReachability(edgeset, 0)  # delta = 0 means no further perturbation
            return ours - best, bReached, bRemoved, oReached
        with concurrent.futures.ThreadPoolExecutor() as executor:
            jobs = [executor.submit(get_robust_for_perturb, perturbed) for perturbed in self.perturbations(delta)]
            for future in concurrent.futures.as_completed(jobs):
                new_robust, newReached, newRemoved, newOurReached = future.result()
                if new_robust > robust:
                    bReached = newReached
                    bRemoved = newRemoved
                    oReached = newOurReached
                    robust = new_robust
        return robust, bReached, bRemoved, oReached

    @timing
    def maxReachability(self, removedEdges: list[Edge], delta: int = 0) -> int:
        """Determine the largest temporal reachability under any delta
        perturbation. Please think carefully about that sentence before
        deciding to use this function with any non-zero delta value. This
        function does not determine robustness of any sort.

        :param root: the root vertex
        :param removedEdges: edges that are not to be used
        :param delta: the maximum allowed perturbation
        """
        tEdges = self.unnameEdges(removedEdges)
        return max(self.perturbedReachability(v, tEdges, delta) for v in range(1, self._numVerts + 1))
#        res = 0
#        for v in range(1, self._numVerts + 1):
#            res = max(res, self.perturbedReachability(v, tEdges, delta))
#        return res

    def perturbedReachability(self, root: int, toRemove: list[TemporalEdge], delta: int = 0) -> int:
        """Attempt to work out the largest temporal reachability from a given
        root vertex under any delta perturbation. Please think carefully about
        that sentence before deciding to use this function with any non-zero
        delta value. This function does not determine robustness of any sort.

        :param root: the root vertex
        :param removedEdges: edges that are not to be used
        :param delta: the maximum allowed perturbation

        """
        # NOTE: You probably want delta=0
        # TODO possible speed-up by turning removedEdges into a set()

        removedEdges = [(e[0], e[1]) for e in toRemove]

        # We keep a track of which vertices we can reach. However, we always
        # want to visit each vertex at most once, and always as soon as
        # possible.
        # To this end, heap is a list of lists, such that heap[t] is the list
        # of vertices we can reach at time t
        # On top of this, first_arrival is a dict mapping each vertex to the
        # soonest we can reach it.
        heap = []
        first_arrival = {}
        def heappush(vertex, time):
            # If we can already reach this vertex, and reach it at least as soon,
            # ignore this call
            if vertex in first_arrival and first_arrival[vertex] <= time:
                return
            # At this point, either vertex has never been reached, or time is a
            # strictly earlier time at which we reach vertex

            # Make sure heap is long enough that heap[time] exists
            while len(heap) <= time:
                heap.append([])
            # Mark down that we can reach vertex at time
            heap[time].append(vertex)
            # If we previously had an arrival, remove vertex from that list
            if vertex in first_arrival:
                heap[first_arrival[vertex]].remove(vertex)
            # And now mark when we first reach vertex
            first_arrival[vertex] = time
        def visit(v, t):
            # Visit incident vertices
            for e in self.incidentAway(v):
                # Ignore removed edges. Note that we only check the end points,
                # as we may have removed edges, then perturbed the time step of
                # the edge we wanted to remove
                if (e[0], e[1]) in removedEdges:
                    continue
                # work out earliest tp (t prime) we can use e
                if t+1 < e[2] - delta:
                    # Have to wait for e to become active
                    tp = e[2] - delta
                elif t+1 > e[2] + delta:
                    # e[2] deactivates too early regardless of perturbation
                    continue
                else:
                    # Use e as soon as possible, noting that from the above if
                    # statements we know in this branch that
                    # e[2] - delta <= t+1 <= e[2] + delta
                    tp = t+1
                # We can visit e[1] at time tp
                # Note that heappush() takes care of only visiting each vertex
                # once, and as soon as possible
                heappush(e[1], tp)
        # Start at root at t=0
        heappush(root, 0)
        time = 0
        # Max time is luckily the length of the heap
        while time < len(heap):
            # visit the vertices in heap[time]
            for v in heap[time]:
                visit(v, time)
            time += 1
        # And return reachability, which is just the number of vertices we
        # visit
        return len(first_arrival), [v in first_arrival for v in range(0, self.numVerts)]

    def _log_minizinc_result(self, result):
        logging.info(f"Solved to {result.status} with initTime {result.statistics['initTime']},"
                     f" flatTime {result.statistics['flatTime']},"
                     f" solveTime {result.statistics['solveTime']}")

    def kDelMaxReachability(self, k: int = 0) -> list[Edge]:
        to_remove: list[Edge] = []
        model_filename = pkg_resources.resource_filename('tred', 'minizinc-models/max_treach_kdel.mzn')
        model = minizinc.Model(model_filename)
        instance = minizinc.Instance(MZ_SOLVER, model)
        instance["nVerts"] = self.numVerts
        instance["nEdges"] = self.numEdges
        instance["maxTime"] = self.maxTime
        instance["from"] = self.edgesFrom
        instance["to"] = self.edgesTo
        instance["active"] = self.edgesActive
        instance["k"] = k
        result = instance.solve()
        self._log_minizinc_result(result)
        return (result['numReached'],
                result['nReachable'],
                self.renameEdges([e for index, e in enumerate(self.edges) if not result['es'][index]])
                )
