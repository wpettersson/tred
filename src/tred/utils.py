

import logging
import time

def timing(function):
    def wrap(*args, **kwargs):
        time1 = time.monotonic_ns()
        ret = function(*args, **kwargs)
        time2 = time.monotonic_ns()
        logging.info(f"{function.__name__} took {(time2-time1)/1000:.3f}ms")
        return ret
    return wrap

